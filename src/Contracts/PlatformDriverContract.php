<?php
/**
 * Project:     Push Notifications
 * Author:      planet17
 */

namespace Planet17\PushNotifications\Contracts;


interface PlatformDriverContract
{
    public static function getSelfName():string;
    public function createPayload($push):Pushes\BasePushContract;
}
