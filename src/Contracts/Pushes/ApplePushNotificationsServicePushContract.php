<?php
/**
 * Project:     Push Notifications
 * Author:      planet17
 */

namespace Planet17\PushNotifications\Contracts\Pushes;


interface ApplePushNotificationsServicePushContract extends BasePushContract
{
    public function getReceiver():string;
    public function getPayloadLength():int;
}
