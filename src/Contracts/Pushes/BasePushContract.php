<?php
/**
 * Project:     Push Notifications
 * Author:      planet17
 */

namespace Planet17\PushNotifications\Contracts\Pushes;


interface BasePushContract
{
    public function getPayload():string;
}
