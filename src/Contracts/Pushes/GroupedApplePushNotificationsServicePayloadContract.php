<?php
/**
 * Project:     Push Notifications
 * File:        GroupedApplePushNotificationsServicePayloadContract.php
 * Author:      planet17
 * DateTime:    M11.D15.2018 21:05
 */

namespace Planet17\PushNotifications\Contracts\Pushes;


interface GroupedApplePushNotificationsServicePayloadContract
{
    public function receiverIsCollection():bool;
}
