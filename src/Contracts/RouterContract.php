<?php
/**
 * Project:     Push Notifications
 * Author:      planet17
 */

namespace Planet17\PushNotifications\Contracts;


/**
 * Interface RouterContract
 *
 * @package Planet17\PushNotifications\Contracts
 */
interface RouterContract
{
    public function getRouted(array $list):array;
    public function getChannel(array $options):string;
    public function getPlatform(array $options):int;
}
