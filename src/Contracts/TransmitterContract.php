<?php
/**
 * Project:     Push Notifications
 * Author:      planet17
 */

namespace Planet17\PushNotifications\Contracts;


interface TransmitterContract
{
    public function send(IPush ... $pushes):ResponseContract;
}
