<?php
/**
 * Project:     Push Notifications
 * File:        NotFoundAddedObjectException.php
 * Author:      planet17
 * DateTime:    M11.D15.2018 16:24
 */

namespace Planet17\PushNotifications\Exceptions;


abstract class NotFoundAddedObjectException extends \InvalidArgumentException
{
    public function __construct(string $key)
    {
        $message = 'Not found included ' . $this->getObjectName() . ' by key: ' . $key;
        parent::__construct($message, 0, null);
    }

    abstract protected function getObjectName():string;
}
