<?php
/**
 * Project:     Push Notifications
 * Author:      planet17
 */

namespace Planet17\PushNotifications\Exceptions;


class NotFoundChannelException extends NotFoundAddedObjectException
{
    protected function getObjectName():string
    {
        return 'Channel';
    }
}
