<?php
/**
 * Project:     Push Notifications
 * File:        NotFoundContractException.php
 * Author:      planet17
 * DateTime:    M11.D15.2018 16:25
 */

namespace Planet17\PushNotifications\Exceptions;


class NotFoundContractException extends NotFoundAddedObjectException
{
    protected function getObjectName():string
    {
        return 'Contract';
    }
}
