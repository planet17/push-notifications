<?php
/**
 * Project:     Push Notifications
 * File:        ApiHelper.php
 * Author:      planet17
 * DateTime:    M11.D15.2018 17:15
 */

namespace Planet17\PushNotifications\Mailing;


class ApiHelper
{
    const FCM                         = 'FCM';

    const APNS                        = 'APNS';

    const LENGTH_TOKEN_STRING         = [
        self::FCM  => 152,
        self::APNS => 64,
    ];

    const LIMIT_RECEIVERS_PER_REQUEST = [self::FCM => 1000];

    const IMPLEMENTED_API             = [
        self::FCM => true,
        self::APNS => false
    ];
}
