<?php
/**
 * Project:     Push Notifications
 * Author:      planet17
 */

namespace Planet17\PushNotifications\Mailing;

use Planet17\PushNotifications\Contracts\Pushes\BasePushContract as Push;
use Planet17\PushNotifications\Contracts\TransmitterContract;
use Planet17\PushNotifications\Exceptions\NotFoundChannelException;

/**
 * Class PushManager
 *
 * @package Planet17\PushNotifications\Mailing
 */
class PushManager
{
    const CONTRACTS_OPTION_TRANSMITTER     = 'transmitter';

    private $transmitters    = [];


    public function setChannel(string $channelName, TransmitterContract $transmitter)
    {
        $this->transmitters[ $channelName ] = $transmitter;
    }


    public function getChannels():array
    {
        return array_keys($this->transmitters);
    }


    public function send(string $channelName, Push ... $pushes)
    {
        if (!isset($this->transmitters[$channelName])){
            throw new NotFoundChannelException($channelName);
        }

        /** @var TransmitterContract $transmitter */
        $transmitter = $this->transmitters[$channelName];

        foreach ($pushes as $push) {
            $transmitter->send($push);
        }
    }
}
