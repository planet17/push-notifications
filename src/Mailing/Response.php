<?php
/**
 * Project:     Push Notifications
 * Author:      planet17
 */

namespace Planet17\PushNotifications\Mailing;


use Planet17\PushNotifications\Contracts\ResponseContract;


/**
 * Class Response
 *
 * @package Planet17\PushNotifications\Mailing
 */
class Response implements ResponseContract
{

}
