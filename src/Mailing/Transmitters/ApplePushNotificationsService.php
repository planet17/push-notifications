<?php
/**
 * Project:     Push Notifications
 * File:        ApplePushNotificationsService.php
 * Author:      planet17
 * DateTime:    M11.D20.2018 14:11
 */

namespace Planet17\PushNotifications\Mailing\Transmitters;


use Planet17\PushNotifications\Contracts\Pushes\BasePushContract as Push;
use Planet17\PushNotifications\Contracts\Pushes\ApplePushNotificationsServicePushContract as APNSPush;


/**
 * Class ApplePushNotificationsService - Type of transmitter for APNS.
 *
 * @package Planet17\PushNotifications\Mailing\Transmitters
 */
final class ApplePushNotificationsService extends Base
{
    /**
     * @var string $pathToCertificate
     */
    protected $pathToCertificate;

    /**
     * @var $uri - Link to api.
     */
    protected $uri;

    /**
     * @var $client - Socket Client.
     */
    protected $client;

    CONST SERVERS_URI = [
        'message' => [
            'production' => 'tls://gateway.sandbox.push.apple.com:2195',
            'test'       => 'tls://gateway.push.apple.com:2195',
        ],
    ];

    /**
     * ApplePushNotificationsService constructor.
     *
     * @param         $responseFactory
     * @param string  $pathToCertificate
     * @param boolean $isDev
     */
    public function __construct($responseFactory, string $pathToCertificate, bool $isDev)
    {
        parent::__construct($responseFactory);
        $this->setCertificate($pathToCertificate);
        $env       = $isDev ? 'test' : 'production';
        $this->uri = self::SERVERS_URI['message'][ $env ];
    }


    protected function getConcretePushInterface():string
    {
        return APNSPush::class;
    }


    /**
     * @param string $pathToFile
     */
    protected function setCertificate(string $pathToFile)
    {
        if ( !$pathToFile) {
            throw new \InvalidArgumentException('Must been provided certificate for APNS');
        }

        if ( !file_exists($pathToFile)) {
            throw new \InvalidArgumentException('Can\'t find file by provided path');
        }

        $this->pathToCertificate = $pathToFile;
    }


    protected function setPush(Push $push)
    {
        var_dump($push instanceof APNSPush);
        $this->Push = $push;
    }

    /**
     * @throws \Exception
     */
    protected function request()
    {
        $this->initClient();

        $return = fwrite($this->client, $this->packPayload());

        if ($return === false) {
            throw new \Exception('Все пиздец');
        }

        $null = null;
        $read = [$this->client];

        if (0 < stream_select($read, $null, $null, 1, 0)) {
            $result = fread($this->client, 6);
            if ($result) {
                $result = unpack('Ccmd/Cerrno/Nid', $result);
            }

            $this->response = [
                'result' => $result,
                'null'   => $null,
            ];

            var_dump($this->response);
        }

        $this->closeClient();
    }

    protected function initClient()
    {
        $context = stream_context_create();
        stream_context_set_option($context, 'ssl', 'local_cert', $this->pathToCertificate);
        $this->client = stream_socket_client($this->uri, $error, $errorString, 5, STREAM_CLIENT_CONNECT, $context);
    }

    protected function closeClient()
    {
        /*socket_close($socket);*/
        fclose($this->client);
    }

    protected function packPayload():string
    {
        $message = \chr(0) . \chr(0) . \chr(32);
        $message .= pack('H*', $this->push->getReceiver());
        $message .= \chr(0) . \chr(\strlen($this->push->getPayload()));
        $message .= $this->push->getPayload();

        return $message;
    }
}
