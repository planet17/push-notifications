<?php
/**
 * Project:     Push Notifications
 * Author:      planet17
 */

namespace Planet17\PushNotifications\Mailing\Transmitters;

use Planet17\PushNotifications\Contracts\Pushes\BasePushContract as Push;
use Planet17\PushNotifications\Contracts\Pushes\ApplePushNotificationsServicePushContract as APNSPush;
use Planet17\PushNotifications\Contracts\Pushes\FirebaseCloudMessagePushContract as FCMPush;
use Planet17\PushNotifications\Contracts\ResponseContract;
use Planet17\PushNotifications\Mailing\Response;


abstract class Base
{
    /**
     * @var Push|FCMPush|APNSPush $push
     */
    protected $push;
    protected $response;

    protected $responseFactory;


    public function __construct($responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }

    /**
     * @param Push|FCMPush|APNSPush $push
     *
     * @return ResponseContract
     */
    public function send(Push $push):ResponseContract
    {
        $this->setPush($push);
        $this->request();
        /** @var ResponseContract $response */
        $response = $this->responseFactory->build($this->response);
        return $response;
    }

    /**
     * @param Push|FCMPush|APNSPush $push
     */
    protected function setPush(Push $push)
    {
        $concreteInterface = $this->getConcretePushInterface();
        if ( !($push instanceof $concreteInterface)) {
            throw new \InvalidArgumentException('Wrong type of provided Push');
        }

        $this->push = $push;
    }

    abstract protected function getConcretePushInterface():string;

    abstract protected function request();
}
