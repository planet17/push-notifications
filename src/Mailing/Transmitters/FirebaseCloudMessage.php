<?php
/**
 * Project:     Push Notifications
 * File:        FirebaseCloudMessage.php
 * Author:      planet17
 * DateTime:    M11.D20.2018 14:12
 */

namespace Planet17\PushNotifications\Mailing\Transmitters;


use Planet17\PushNotifications\Contracts\Pushes\FirebaseCloudMessagePushContract as FCMPush;


/**
 * Class FirebaseCloudMessage - Type of transmitter for FCM.
 *
 * @package Planet17\PushNotifications\Mailing\Transmitters
 */
final class FirebaseCloudMessage extends Base
{
    /**
     * @const string Server URI
     */
    const SERVER_URI = 'https://fcm.googleapis.com/fcm/send';

    private $key;


    public function __construct($responseFactory, string $key)
    {
        parent::__construct($responseFactory);

        if ( !$key) {
            throw new \InvalidArgumentException('Key must been provided!');
        }
        $this->key = $key;
    }


    protected function getConcretePushInterface():string
    {
        return FCMPush::class;
    }


    protected function request()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::SERVER_URI);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getHttpHeaders());
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->push->getPayload());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $response = curl_exec($ch);
        curl_close($ch);

        $this->response = json_decode($response);
    }


    protected function getApiKey():string
    {
        return $this->key;
    }


    protected function getHttpHeaders():array
    {
        return ['Content-Type: application/json', 'Authorization: key=' . $this->getApiKey()];
    }
}
