<?php
/**
 * Project:     Push Notifications
 * Author:      planet17
 */

namespace Planet17\PushNotifications\Pushes;


use Planet17\PushNotifications\Contracts\Pushes\ApplePushNotificationsServicePushContract;


/**
 * Class ApplePushNotificationService
 *
 * @package Planet17\PushNotifications\Pushes
 */
abstract class ApplePushNotificationService extends Base implements ApplePushNotificationsServicePushContract
{
    public function getReceiver():string
    {
        return 'tokenakdfnwelfjlkej';
    }

    public function getPayloadLength():int
    {
        if (!$this->payload) {
            $this->constructPayload();
        }

        return $this->payload;
    }

    /* abstract protected function constructPayload(); */
}
