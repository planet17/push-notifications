<?php
/**
 * Project:     Push Notifications
 * Author:      planet17
 */

namespace Planet17\PushNotifications\Pushes;


use Planet17\PushNotifications\Contracts\Pushes\BasePushContract;


/**
 * Class BasePush
 *
 * @package Planet17\PushNotifications\Pushes
 */
abstract class Base implements BasePushContract
{
    protected $payload;


    /**
     * Method must implement return all payload information like string for
     * transmitting via some of Push Services.
     *
     * @return string
     */
    public function getPayload():string
    {
        if (!$this->payload) {
            $this->constructPayload();
        }

        return \strlen($this->payload);
    }

    abstract protected function constructPayload();
}
