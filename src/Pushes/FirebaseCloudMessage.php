<?php
/**
 * Project:     Push Notifications
 * Author:      planet17
 */

namespace Planet17\PushNotifications\Pushes;


use Planet17\PushNotifications\Contracts\Pushes\FirebaseCloudMessagePushContract;


/**
 * Class FirebaseCloudMessage
 *
 * @package Planet17\PushNotifications\Pushes
 */
abstract class FirebaseCloudMessage extends Base implements FirebaseCloudMessagePushContract
{
    public function getPayload():string
    {
        return json_encode(['registrations_ids' => 'erwtr']);
    }

    /* abstract protected function constructPayload(); */
}
