<?php
/**
 * Project:     Push Notifications
 * Author:      planet17
 */


namespace Planet17\PushNotifications\Mailing\Transmitters;


use PHPUnit\Framework\TestCase;


class FirebaseCloudMessageTest extends TestCase
{
    private $key;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        $configPath = dirname(__DIR__, 3) . '/config/autodoc/accounts.php';
        $this->key = (require_once $configPath)['fcmKeys']['tlsde'];
        parent::__construct($name, $data, $dataName);
    }


    public function testCreate()
    {
        $transmitter = new FirebaseCloudMessage($this->key);
        foreach ([[new FCMPush(), new FCMPush(), new ApnsPush()]] as $push) {
            $transmitter->send($push);
        }

        $this->assertEquals('test', 'test');
    }
}
